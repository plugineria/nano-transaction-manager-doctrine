<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfDdd\TransactionManager\Domain\TransactionManagerInterface;

class DoctrineClearingTransactionManagerDecorator implements TransactionManagerInterface
{
    public function __construct(
        private TransactionManagerInterface $originalTransactionManager,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function wrapInTransaction(callable $callback): void
    {
        $this->originalTransactionManager->wrapInTransaction($callback);
        $this->entityManager->clear();
    }
}
