<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Factory;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\DoctrineClearingTransactionManagerDecorator;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\DoctrineTransactionManager;

class ClearingTransactionManagerFactory
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function __invoke(): DoctrineClearingTransactionManagerDecorator
    {
        return new DoctrineClearingTransactionManagerDecorator(
            new DoctrineTransactionManager($this->entityManager),
            $this->entityManager,
        );
    }
}
