USER_NAME:=$(shell whoami)
USER_ID:=$(shell id -u)
GROUP_ID:=$(shell id -g)

PHP_VERSION:=8.0

DOCKER_COMPOSE := PHP_VERSION=$(PHP_VERSION) \
	USER_NAME=$(USER_NAME) \
	USER_ID=$(USER_ID) \
	GROUP_ID=$(GROUP_ID) \
	docker compose

DOCKER_COMPOSE_PHP := $(DOCKER_COMPOSE) run --rm --interactive --tty php

composer-install:
	$(DOCKER_COMPOSE_PHP) composer install

composer-validate:
	$(DOCKER_COMPOSE_PHP) composer validate

test-functional:
	@make pg-load
	$(DOCKER_COMPOSE_PHP) vendor/bin/phpunit

cs-fixer:
	$(DOCKER_COMPOSE_PHP) vendor/bin/php-cs-fixer fix --allow-risky=yes ./

build:
	$(DOCKER_COMPOSE) build --no-cache --pull

push:
	$(DOCKER_COMPOSE) push

start:
	$(DOCKER_COMPOSE) up -d

down:
	$(DOCKER_COMPOSE) down

bash:
	$(DOCKER_COMPOSE_PHP) bash

pg-dump:
	mkdir -p docker/postgres/
	$(DOCKER_COMPOSE) exec postgres pg_dump -Utest --clean testdb > docker/postgres/dump.sql

pg-load:
	 $(DOCKER_COMPOSE) exec -it postgres psql testdb test -f /docker-entrypoint-initdb.d/dump.sql

