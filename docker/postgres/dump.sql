--
-- PostgreSQL database dump
--

-- Dumped from database version 16.1
-- Dumped by pg_dump version 16.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP TABLE IF EXISTS public.accountbalance;
SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: accountbalance; Type: TABLE; Schema: public; Owner: test
--

CREATE TABLE public.accountbalance (
    id uuid NOT NULL,
    amount integer
);


ALTER TABLE public.accountbalance OWNER TO test;

--
-- Data for Name: accountbalance; Type: TABLE DATA; Schema: public; Owner: test
--

COPY public.accountbalance (id, amount) FROM stdin;
e2bdac5c-86a1-4130-9a11-5e439cfb36f5	0
f9ae8ca3-5933-407a-b106-e1b00ee00b49	20
a2946cc3-61d9-4497-b982-0b401bcae92f	100
\.


--
-- Name: accountbalance accountbalance_pk; Type: CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY public.accountbalance
    ADD CONSTRAINT accountbalance_pk PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

