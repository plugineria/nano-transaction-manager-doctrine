<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit;

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Tools\DsnParser;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use Ramsey\Uuid\Doctrine\UuidType;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

class EntityManagerFactory
{
    private const CUSTOM_TYPES = [
        UuidType::class,
        IntegerType::class,
    ];

    public static function create(): EntityManager
    {
        $cache = new ArrayAdapter();
        $config = ORMSetup::createAttributeMetadataConfiguration(
            paths: [__DIR__ . '/Entity'],
            isDevMode: true,
            cache: $cache
        );

        $connection = DriverManager::getConnection(array_merge(
            (new DsnParser())->parse($_ENV['DATABASE_DSN']),
            ['driver' => $_ENV['DATABASE_DRIVER']]
        ), $config);

        self::initCustomTypes();

        return new EntityManager($connection, $config);
    }

    private static function initCustomTypes(): void
    {
        foreach (self::CUSTOM_TYPES as $className) {
            if (!Type::hasType($className)) {
                Type::addType($className, $className);
            }
        }
    }
}
