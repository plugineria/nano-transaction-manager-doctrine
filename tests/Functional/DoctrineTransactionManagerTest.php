<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Functional;

use Doctrine\DBAL\Exception\SyntaxErrorException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\EntityManagerClosed;
use FriendsOfDdd\TransactionManager\Domain\LogicTerminationException;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\DoctrineTransactionManager;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\AccountBalance;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\AccountBalanceId;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\Amount;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\EntityManagerFactory;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class DoctrineTransactionManagerTest extends TestCase
{
    private const ACCOUNT_BALANCE_ID_WITH20 = 'f9ae8ca3-5933-407a-b106-e1b00ee00b49';

    private DoctrineTransactionManager $transactionManager;
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = EntityManagerFactory::create();
        $this->transactionManager = new DoctrineTransactionManager(
            $this->entityManager
        );

        $this->entityManager->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->entityManager->rollback();
    }

    public function testCallEmptyCallbackDoesNotBlowUp(): void
    {
        $this->transactionManager->wrapInTransaction(static fn () => null);

        self::assertTrue(true, 'No exceptions occurred');
    }

    public function testSaving2AccountBalancesUpdatesTheValuesInDb(): void
    {
        // Arrange
        $accountBalances  = [
            (new AccountBalance(AccountBalanceId::new())),
            (new AccountBalance(AccountBalanceId::new())),
        ];
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);

        // Act
        $this->transactionManager->wrapInTransaction(function () use ($accountBalances) {
            foreach ($accountBalances as $accountBalance) {
                $this->entityManager->persist($accountBalance);
                $this->entityManager->flush();
            }
        });

        // Assert
        $this->entityManager->clear();
        foreach ($accountBalances as $accountBalance) {
            $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());

            self::assertEquals($accountBalance->getAmount(), $savedAccountBalance->getAmount());
        }
    }

    public function testSavingAccountBalanceAndExceptionAfterItRollsBackTheAccountBalance(): void
    {
        // Arrange
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);
        $accountBalance = $accountBalanceRepository->find(self::ACCOUNT_BALANCE_ID_WITH20);
        $expectedException = new RuntimeException('Something went wrong');

        // Assert
        $this->expectExceptionObject($expectedException);

        // Act
        $this->transactionManager->wrapInTransaction(function () use ($accountBalance, $expectedException) {
            $accountBalance->withdraw(Amount::fromInt(15));
            $this->entityManager->persist($accountBalance);
            $this->entityManager->flush();

            throw $expectedException;
        });

        // Assert
        $this->entityManager->clear();
        $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());
        self::assertEquals(Amount::fromInt(5), $accountBalance->getAmount());
        self::assertEquals(Amount::fromInt(20), $savedAccountBalance->getAmount());
    }

    public function testSavingAccountBalanceAndInvalidSqlAfterRollsBackAccountBalance(): void
    {
        // Arrange
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);
        $accountBalance = $accountBalanceRepository->find(self::ACCOUNT_BALANCE_ID_WITH20);

        // Assert
        $this->expectException(SyntaxErrorException::class);

        // Act
        $this->transactionManager->wrapInTransaction(function () use ($accountBalance) {
            $accountBalance->withdraw(Amount::fromInt(15));
            $this->entityManager->persist($accountBalance);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->executeQuery(
                <<<'SQL'
                    UPDATE "not_existing_table" 
                    SET value=1'
                    SQL
            );
        });

        // Assert
        $this->entityManager->clear();
        $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());
        self::assertEquals(Amount::fromInt(5), $accountBalance->getAmount());
        self::assertEquals(Amount::fromInt(20), $savedAccountBalance->getAmount());
    }

    public function testConnectionOpenAfterTransaction(): void
    {
        // Arrange
        $this->transactionManager->wrapInTransaction(static fn () => null);

        // Act
        $this->entityManager->flush();

        // Assert
        self::assertTrue(true, 'Should not throw an error');
    }

    public function testConnectionIsClosedAfterRandomException(): void
    {
        // Arrange
        try {
            $this->transactionManager->wrapInTransaction(static function () {
                throw new RuntimeException('some error');
            });
        } catch (RuntimeException) {
            self::assertTrue(true);
        }

        // Assert
        $this->expectException(EntityManagerClosed::class);

        // Act
        $this->entityManager->flush();
    }

    public function testConnectionIsOpenAfterLogicTerminationException(): void
    {
        // Arrange
        try {
            $this->transactionManager->wrapInTransaction(static function () {
                throw new LogicTerminationException('some error');
            });
        } catch (LogicTerminationException) {
            self::assertTrue(true);
        }

        // Act
        $this->entityManager->flush();
    }

    public function testPersistEntityInCallbackWithoutInlineFlushSavesEntityToDb(): void
    {
        // Arrange
        $accountBalances  = [
            (new AccountBalance(AccountBalanceId::new())),
            (new AccountBalance(AccountBalanceId::new())),
        ];
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);

        // Act
        $this->transactionManager->wrapInTransaction(function () use ($accountBalances) {
            foreach ($accountBalances as $accountBalance) {
                $this->entityManager->persist($accountBalance);
            }
        });

        // Assert
        $this->entityManager->clear();
        foreach ($accountBalances as $accountBalance) {
            $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());

            self::assertEquals($accountBalance->getAmount(), $savedAccountBalance->getAmount());
        }
    }
}
